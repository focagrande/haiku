CREATE OR REPLACE FUNCTION haiku.haikuline(i_slbcount int)
RETURNS text[]
LANGUAGE plpgsql
AS
$$
DECLARE

    _slb_remain int;
    _rand int;
    _retval text[];

BEGIN

    _slb_remain = i_slbcount;

    WHILE _slb_remain > 0
    LOOP

        SELECT 1 + trunc(random() * _slb_remain)::integer INTO _rand;

        SELECT array_append(_retval, haiku.randomword(_rand)) INTO _retval;

        _slb_remain = _slb_remain - _rand;

    END LOOP;

    RETURN _retval;


END;
$$;
