CREATE OR REPLACE FUNCTION haiku.randomword(i_slbcount int)
RETURNS text
AS
$$
DECLARE
    _result haiku.words_norm%rowtype;
    _rowcount int;
    _idx int;
    _word text;
BEGIN

    SELECT reltuples INTO _rowcount
    FROM pg_class
    WHERE oid = 'haiku.words_norm'::regclass;

    _idx = 0;

    WHILE _idx = 0
    LOOP

        SELECT wnr.* INTO _result
        FROM (SELECT 1 + trunc(random() * _rowcount)::integer AS wn_id) rnd 
        JOIN haiku.words_norm wnr USING (wn_id);

        IF _result.wn_id IS NULL THEN
            CONTINUE;
        END IF;

        _idx = _result.wn_syllable_count # i_slbcount;

        IF _idx > 0 THEN

            SELECT wn_word_alts[_idx] INTO _word
            FROM haiku.words_norm
            WHERE wn_id = _result.wn_id;

        END IF;
    
    END LOOP;

    RETURN _word;

END;
$$ LANGUAGE plpgsql;
