#!/usr/bin/env python3

import sys

import psycopg2
import pyphen

dic = pyphen.Pyphen(lang='pl_PL')
conn = psycopg2.connect('dbname=haiku user=jspiewak')


def hyph(word):

    return dic.inserted(word).split('-')


def main():

    with conn:
        with conn.cursor() as cur:
            cur.execute('select * from haiku.words_norm where wn_syllable_count is null')
            
            count = 0
            for rec in cur.fetchall():

              slbn = [len(hyph(word)) for word in rec[2]]

              with conn.cursor() as cur_upd:
                  cur_upd.execute('''
                         update haiku.words_norm 
                         set wn_syllable_count = %s 
                         where wn_id = %s
                      ''',
                      (slbn, rec[0]))

              count = count + 1

              if (count % 10000) == 0:
                  print(str(count), '...')
                  conn.commit()

    conn.close()

if __name__ == '__main__':
    sys.exit(main())

