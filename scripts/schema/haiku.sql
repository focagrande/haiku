--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.6
-- Dumped by pg_dump version 9.6.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: haiku; Type: SCHEMA; Schema: -; Owner: jspiewak
--

CREATE SCHEMA haiku;


ALTER SCHEMA haiku OWNER TO jspiewak;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: intarray; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS intarray WITH SCHEMA public;


--
-- Name: EXTENSION intarray; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION intarray IS 'functions, operators, and index support for 1-D arrays of integers';


SET search_path = haiku, pg_catalog;

--
-- Name: haikuline(integer); Type: FUNCTION; Schema: haiku; Owner: jspiewak
--

CREATE FUNCTION haikuline(i_slbcount integer) RETURNS text[]
    LANGUAGE plpgsql
    AS $$
DECLARE

    _slb_remain int;
    _rand int;
    _retval text[];

BEGIN

    _slb_remain = i_slbcount;

    WHILE _slb_remain > 0
    LOOP

        SELECT 1 + trunc(random() * _slb_remain)::integer INTO _rand;

        SELECT array_append(_retval, haiku.randomword(_rand)) INTO _retval;

        _slb_remain = _slb_remain - _rand;

    END LOOP;

    RETURN _retval;


END;
$$;


ALTER FUNCTION haiku.haikuline(i_slbcount integer) OWNER TO jspiewak;

--
-- Name: randomword(integer); Type: FUNCTION; Schema: haiku; Owner: jspiewak
--

CREATE FUNCTION randomword(i_slbcount integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    _result haiku.words_norm%rowtype;
    _rowcount int;
    _idx int;
    _word text;
BEGIN

    SELECT reltuples INTO _rowcount
    FROM pg_class
    WHERE oid = 'haiku.words_norm'::regclass;

    _idx = 0;

    WHILE _idx = 0
    LOOP

        SELECT wnr.* INTO _result
        FROM (SELECT 1 + trunc(random() * _rowcount)::integer AS wn_id) rnd 
        JOIN haiku.words_norm wnr USING (wn_id);

        IF _result.wn_id IS NULL THEN
            CONTINUE;
        END IF;

        _idx = _result.wn_syllable_count # i_slbcount;

        IF _idx > 0 THEN

            SELECT wn_word_alts[_idx] INTO _word
            FROM haiku.words_norm
            WHERE wn_id = _result.wn_id;

        END IF;
    
    END LOOP;

    RETURN _word;

END;
$$;


ALTER FUNCTION haiku.randomword(i_slbcount integer) OWNER TO jspiewak;

--
-- Name: randomword_lt(integer); Type: FUNCTION; Schema: haiku; Owner: jspiewak
--

CREATE FUNCTION randomword_lt(i_slbcount_max integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    _result haiku.words_norm%rowtype;
    _rowcount int;
    _idx int;
    _word text;
    _slbcount int;
BEGIN

    SELECT reltuples INTO _rowcount
    FROM pg_class
    WHERE oid = 'haiku.words_norm'::regclass;

    
    SELECT trunc(random() * i_slbcount_max) + 1 INTO _slbcount;

    _idx = 0;
    
    WHILE _idx = 0
    LOOP

        
        SELECT wnr.* INTO _result
        FROM (SELECT 1 + trunc(random() * _rowcount)::integer AS wn_id) rnd 
        JOIN haiku.words_norm wnr USING (wn_id);

        IF _result.wn_id IS NULL THEN
            CONTINUE;
        END IF;

        _idx = _result.wn_syllable_count # _slbcount;

        IF _idx > 0 THEN

            SELECT wn_word_alts[_idx] INTO _word
            FROM haiku.words_norm
            WHERE wn_id = _result.wn_id;

        END IF;
    
    END LOOP;

    RETURN _word;

END;
$$;


ALTER FUNCTION haiku.randomword_lt(i_slbcount_max integer) OWNER TO jspiewak;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: words; Type: TABLE; Schema: haiku; Owner: jspiewak
--

CREATE TABLE words (
    word_forms text
);


ALTER TABLE words OWNER TO jspiewak;

--
-- Name: words_norm; Type: TABLE; Schema: haiku; Owner: jspiewak
--

CREATE TABLE words_norm (
    wn_id integer NOT NULL,
    wn_word_main text,
    wn_word_alts text[],
    wn_syllable_count integer[]
);


ALTER TABLE words_norm OWNER TO jspiewak;

--
-- Name: words_norm_json; Type: TABLE; Schema: haiku; Owner: jspiewak
--

CREATE TABLE words_norm_json (
    wnj_id integer NOT NULL,
    wnj_word_main text,
    wnj_word_alt jsonb
);


ALTER TABLE words_norm_json OWNER TO jspiewak;

--
-- Name: words_norm_json_wnj_id_seq; Type: SEQUENCE; Schema: haiku; Owner: jspiewak
--

CREATE SEQUENCE words_norm_json_wnj_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE words_norm_json_wnj_id_seq OWNER TO jspiewak;

--
-- Name: words_norm_json_wnj_id_seq; Type: SEQUENCE OWNED BY; Schema: haiku; Owner: jspiewak
--

ALTER SEQUENCE words_norm_json_wnj_id_seq OWNED BY words_norm_json.wnj_id;


--
-- Name: words_norm_wn_id_seq; Type: SEQUENCE; Schema: haiku; Owner: jspiewak
--

CREATE SEQUENCE words_norm_wn_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE words_norm_wn_id_seq OWNER TO jspiewak;

--
-- Name: words_norm_wn_id_seq; Type: SEQUENCE OWNED BY; Schema: haiku; Owner: jspiewak
--

ALTER SEQUENCE words_norm_wn_id_seq OWNED BY words_norm.wn_id;


--
-- Name: words_norm wn_id; Type: DEFAULT; Schema: haiku; Owner: jspiewak
--

ALTER TABLE ONLY words_norm ALTER COLUMN wn_id SET DEFAULT nextval('words_norm_wn_id_seq'::regclass);


--
-- Name: words_norm_json wnj_id; Type: DEFAULT; Schema: haiku; Owner: jspiewak
--

ALTER TABLE ONLY words_norm_json ALTER COLUMN wnj_id SET DEFAULT nextval('words_norm_json_wnj_id_seq'::regclass);


--
-- Name: words_norm_json words_norm_json_pkey; Type: CONSTRAINT; Schema: haiku; Owner: jspiewak
--

ALTER TABLE ONLY words_norm_json
    ADD CONSTRAINT words_norm_json_pkey PRIMARY KEY (wnj_id);


--
-- Name: words_norm words_norm_pkey; Type: CONSTRAINT; Schema: haiku; Owner: jspiewak
--

ALTER TABLE ONLY words_norm
    ADD CONSTRAINT words_norm_pkey PRIMARY KEY (wn_id);


--
-- Name: words_norm_slbl_count; Type: INDEX; Schema: haiku; Owner: jspiewak
--

CREATE INDEX words_norm_slbl_count ON words_norm USING gin (wn_syllable_count);


--
-- PostgreSQL database dump complete
--

